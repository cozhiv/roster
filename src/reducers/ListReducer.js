export const listReducer = (state, action) => {
  switch (action.type) {
  case 'ADD_ITEM':
    return {
      ...state,
      list: state.list.concat({ name: action.name, id: action.id }),
    };
  case 'REMOVE_ITEM':
    return {
      ...state,
      list: state.list.filter((item) => {
        return action.id !== item.id;
      })
    };
  case 'LIST_ITEM':
    return {
      ...state,
      list: state.list
    };
  default:
    throw new Error();
  }
};