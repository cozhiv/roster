/* eslint-disable react/react-in-jsx-scope */
import React, {useState, useEffect, createContext, useReducer } from 'react';
import { listReducer } from '../reducers/ListReducer';
import { v4 as uuidv4 } from 'uuid';

export const ListContext = createContext();

export const ListProvider = ({ children }) => {

  const initialList = [];
  const [state, dispatch] = useReducer(listReducer, {
    list: initialList
  });
  const [name, setName] = useState('');

  useEffect(() => {
    const storage = localStorage.getItem('list');
    if (storage) {
      state.list = JSON.parse(storage);
      dispatch({ type: 'LIST_ITEM'});
    }
    
  }, []);

  const handleChange = (event) => {
    setName(event.target.value);
  };

  const handleAdd = (e) => {
    if (!e.key || e.key === 'Enter') {
      dispatch({ type: 'ADD_ITEM', name, id: uuidv4() });
      localStorage.setItem('list', JSON.stringify(state.list));
      setName('');
    }
  };

  const handleRemove = (id) => {
    return () => {
      dispatch({ type: 'REMOVE_ITEM', id});
      localStorage.setItem('list', JSON.stringify(state.list));
    };
  };

  return (
    <ListContext.Provider
      value={{
        name,
        list: state.list,
        handleAdd,
        handleChange,
        handleRemove
      }}
    >
      {children}
    </ListContext.Provider>
  );

};
