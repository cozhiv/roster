import React from 'react';
import { ListMap } from './ListMap';
import { AddItem } from './AddItem';

export const ListHolder = () => {
  return (
    <div>
      <AddItem/>
      <ListMap/>
    </div>
  );
};