import React from 'react';
import PropTypes from 'prop-types';
import { Button, Input, Space } from 'antd';
import { useContext } from 'react';
import { ListContext } from '../context/ListContext';


export const AddItem = () => {
  const { handleAdd, handleChange, name } = useContext(ListContext);
  return (
    <div>
      <Space direction="vertical" size="middle">
        <Space.Compact
          style={{
            width: '100%',
          }}
        >
          <Input value={name}
            onChange={handleChange}
            onKeyDown={handleAdd}
            defaultValue="Combine input and button" />
          <Button type="primary" onClick={handleAdd}>Add</Button>
        </Space.Compact>
      </Space>
    </div>
  );
};
