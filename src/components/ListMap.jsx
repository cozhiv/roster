import React from 'react';
import PropTypes from 'prop-types';
import { Avatar, List, Space, Button, Tooltip} from 'antd';
import { PoweroffOutlined } from '@ant-design/icons';
import { Link } from 'react-router-dom';
import { useContext } from 'react';
import { ListContext } from '../context/ListContext';



export const ListMap = () => {
  const {list, handleRemove } = useContext(ListContext);
  return (
    <>
      <Space
        direction="vertical"
        style={{
          marginBottom: '20px',
          width: '100%',
        }}
        size="middle"
      >
        <List
          pagination={{  
            position: 'bottom',
            align: 'center',
            pageSize: 3,
          }}
          itemLayout="vertical"
          size="large"
          dataSource={list}
          footer={
            <div>
              <b>Alivy design</b> Use roster to note your way forward.
            </div>
          }
          renderItem={(item, index) => (
            <List.Item>
              <List.Item.Meta
                key={item.id}
                avatar={<Avatar src={`https://api.dicebear.com/7.x/miniavs/svg?seed=${index}`} />}
                title={<Link to={'item/'} state={item}>{item.name}</Link>}
                description={'Avatar type #'+index}
              /> 
              <Tooltip title="remove">
                <Button type="primary" shape="circle" icon={<PoweroffOutlined />} onClick={handleRemove(item.id)} />
              </Tooltip>
            </List.Item>
          )}
        />
      </Space>
    </>
  );
};


// ListMap.propTypes = {
//   list: PropTypes.array.isRequired,
//   handleRemove: PropTypes.func.isRequired
// };