import React from 'react';
import { ListHolder } from './components/ListHolder';
import { ItemDetails } from './components/ItemDetails';
import { BrowserRouter, Routes, Route, Outlet} from 'react-router-dom';
import { ListProvider } from './context/ListContext';


const Layout = () => {
  return (
    <>
      <nav>
        <h1>Roster</h1>
      </nav>
      <Outlet />
    </>
  );
};

const App = () => {
  return <div>
    <ListProvider>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Layout/>}>
            <Route index element={<ListHolder />} />
            <Route path="item" element={<ItemDetails />} />
          </Route>
        </Routes>
      </BrowserRouter>
    </ListProvider>
  </div>;
};

export default App;